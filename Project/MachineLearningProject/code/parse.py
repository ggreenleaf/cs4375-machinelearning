# -*- coding: utf-8 -*-
from sklearn import tree
from sklearn.metrics import confusion_matrix
import pydot
from StringIO import StringIO
#hardcoded files for now

train_nmv = "../data/train.nmv.txt"
test_nmv = "../data/prelim-nmv-noclass.txt"
#features is the class attributes
#labels are the classification
#needed for the scikit decesiontree function


def get_features_and_labels(fname):
    with open(fname) as f:
        lines = [line.rstrip("\n") for line in f if not line.isspace()]
        
    lines = [list(map(float,line.split())) for line in lines] #ignore the last element which is the ? class
    labels = []
    features = []
    for line in lines:
        feature,label = line[:-1],line[-1]
        features.append(feature)
        labels.append(label)
    return features, labels     

def get_features(fname):
    with open(fname) as f:
        lines = [line.rstrip("\n") for line in f if not line.isspace()]

    return [list(map(float,line.split()[:-1])) for line in lines]
	    	
def learn_tree(features,labels):
        clf = tree.DecisionTreeClassifier()
        clf = clf.fit(features,labels)
        return clf

def predict(clf,X):
    res = clf.predict([X])
    return res

def get_accuracy(predicted,actual):
    total = 0
    assert len(predicted) == len(actual) #ensure same length     
    for i in range(len(predicted)):
        if predicted[i] == actual[i]:
            total += 1
    return (float(total) / len(actual)) * 100        

def save_results(results):
    with open("../results/prelim_test.txt","w") as f:
        
		f.write("\n".join(list(map(str,map(int,results)))))
		
	
if __name__ == "__main__":
    features,labels = get_features_and_labels(train_nmv)
    test_features = get_features(test_nmv)
    learned_tree = learn_tree(features,labels)
    
    results = [predict(learned_tree,x)[0] for x in test_features]
    #print results
    save_results(results)
   # tree.export_graphviz(learned_tree,out_file='tree.dot')
   # dotfile = StringIO()
  #  tree.export_graphviz(learned_tree,out_file=dotfile)
 #   pydot.graph_from_dot_data(dotfile.getvalue()).write_png('tree.png')
    
	
    
    #accuracy = get_accuracy(results,labels)
    #print "{}%".format(accuracy)
    

    
              