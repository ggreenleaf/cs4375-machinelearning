This program implements the ID3 decision tree algorithm we learned in class. 


The program is run using python3.5 However when running the program I believe that any 3.x version of python will work. 
Running the program in any 2.x version of python will not work because the print statement is a function in version 3.x
as well as when doing integer division in python3.x will result in a float while in python2.x will result in an integer that is 

in python 3.x 
   1 / 5 =  .2
   
in python 2.x
  1 / 5 = 0

But to ensure the program runs correctly please run using python3.5

Contents

Parse.py
    The Parse source contains the code that will parse the the training and test data
    into instances that we can then run  the algorithm on
    
Utils.py
    This file contains two functions that are commonly used. 
    The entropy function which calculates the entropy given a list of probabilities
    and the log2 function. In python using the math.log function is not an option because in some cases
    we may have to take the log_2(0) which throws a value error in python
    in our log2 function we catch this valueerror and return zero so log_2(0) = 0

Decision_tree.py
    This is the main section of the algorithm. There are two classes defined in this file
    The Node class and the ID3Algorithm class. 
    The Node class is a single node in the decision tree
    and the ID3Algorithm class is a object we create to run the algorithm and view results. 
    Both classes are documented further in there definition.   
      
Main.py
    The File we use to run the program. This file takes 2 command line arguments.
       The first argument is the training data
       The second argument is the test data
    To run the program run the following from the command line after unzipping the project.
    
    python code/main.py [path/to/training.data] [path/to/test.dat]
    
    if path is not given an error will occur. 
    
learning_curve.py

    The learning_curve file is a file used to generate the accuracy of running the algorithm on different size training sets
    This file is not needed to run the main algorithm but was used to automate part d of the assignment. 
    
learning curve.pdf

    This is a plot of the training information we gained from the learning_curve.py program
   
    This learning Curve looks like the usual properties of a learning curve.
    and looks similar to a log function how ever we start to approach a max accuracy around 87% starting at training 400 instances
    therefore the other 400 instances would not be needed to train because the accuracy does not increase. 
    