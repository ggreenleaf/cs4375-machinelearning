from utils import entropy
from parse import get_training_data


# A node object represents a single node in the decision tree
# The data a node is the attributes it has left to choose from for another split
# the left and Right Node of the tree. If None then we know its a leaf
# The entropy for the given node. will be used to find the Information gain for the given attributes
# This is the main part of the program that implements the ID3 Algorithm


class Node(object):
    """Represents a single Node in the decision tree
    
    Attributes::
        left (Node): Left child of thhis
        right (Node): Right child of this
        instances (list): training instances in the current node
        name (str): name of the node normally the attribute that generated it
        value (int): if the node is pure then value is the final classification result
        classification(int): when choosing an attribute we need to know the classification of the attribute(0/1)
    """
    def __init__(self,left=None,
                    right=None,
                    instances=None,
                    attributes=None,
                    name=None,
                    value=None, 
                    classification=None):
        self.name = name
        self.value = value
        self.classification = classification #will only be set on leaf nodes
        self.attributes = attributes
        self.instances = instances

        self.positive_instances = [i for i in instances if i.classification]
        self.negative_instances = [i for i in instances if not i.classification]
        self.left = None 
        self.right = None
        self.entropy = 0
        self.calculate_entropy()
        
    def calculate_entropy(self):
        """calculate the current entropy of the node"""
        if len(self.instances) > 0: 
            total = len(self.instances)
            # positives = [x for x in self.instances if x.classification]
            # negatives = [x for x in self.instances if not x.classification]
            probabilities = [len(self.positive_instances)/ total, len(self.negative_instances)/total]
            self.entropy = entropy(probabilities)
        
        
        
class ID3Algorithm(object):
    """A implementation of the ID3 decision tree algorithm
    
    attributes:
        trainfile (str): path to training file data
        testfile (str): path to test file data
        i (int) when changing the size of training data set we can pass an optional i to limit its size
        root (Node): The root node of the decision tree holds all instances    
    """
    
    def __init__(self,trainfile,testfile,i=None):
        self.trainfile = trainfile
        self.testfile = testfile
        training_info = get_training_data(trainfile)
        if i:
            self.instances = training_info.data[:i]
        else:
            self.instances = training_info.data
        
        self.test_instances = get_training_data(testfile).data
        self.root = Node(instances=self.instances,
                            attributes=training_info.attribute_names)       

    def start(self):
        """starts the recursive call to the learn_tree method"""
        self.learn_tree(self.root)      
        
    def learn_tree(self, node):
        """The ID3 algorithm that learns the tree based off the algorithm we learned in class"""
        if node:
            left, right= None, None
            if not is_pure_node(node):
                attr, max_gain = self.max_information_gain(node)
                
                #after finding max gain we set up two nodes and recurively call learn_tree
                instances1 = self.get_instances_by_attribute(node.instances, attr, 1)
                instances0 = self.get_instances_by_attribute(node.instances, attr, 0)
                new_attrs = node.attributes[:]
                new_attrs.remove(attr)
                if len(instances0) > 0: 
                    left = Node(instances=instances0, 
                                attributes=new_attrs, 
                                name=attr,
                                classification=0)
                
                if len(instances1) > 0:            
                    right = Node(instances=instances1, 
                                attributes=new_attrs, 
                                name=attr,
                                classification=1)
                            
                node.left = left
                node.right = right
                self.learn_tree(node.left)
                self.learn_tree(node.right) 
            

    def information_gain(self,node, attr):
        """calculate the information gain for some attribute attr"""
        #nested lists is a list of instances with attr value the same
        instances = [self.get_instances_by_attribute(node.instances,attr,i) for i in range(2)]
        #check if all instances have the same attributue value resulting in no IG
        if any(len(sub_instances) == 0 for sub_instances in instances):
            return (attr, 0)
        
        pc0 = len(instances[0]) / len(instances[0] + instances[1])
        pc1 = len(instances[1]) / len(instances[0] + instances[1])
        
        probs0 = self.get_attribute_probabilties(instances[0])
        probs1 = self.get_attribute_probabilties(instances[1])    
            
        ig = node.entropy - (pc0 * entropy(probs0) + pc1*entropy(probs1))
        return (attr,ig)
            
    def get_attribute_probabilties(self,instances):
        """return a list of probilities of instance | attr"""
        
        positives = self.get_instances_by_class(instances,1)
        negatives = self.get_instances_by_class(instances,0)
        
        total = len(positives + negatives)
        pos = len(positives) / total
        neg = len(negatives) / total
        probs = [pos,neg]
        return probs
    
    def max_information_gain(self, node):
        """return the max IG for a given node and possible attributes to choose from"""
        gain_list = [self.information_gain(node, attr) for attr in node.attributes]      
        return max(gain_list,key=lambda t: t[1])
    
    def get_instances_by_attribute(self,instances, attr, attr_value):
        """returns a tuple of lists with all the instances with attr"""      
        return [x for x in instances if x.attributes[attr] == attr_value]
    
    def get_instances_by_class(self, instances, classification):
        """returns all instacnes whose classification is either a 0 or 1"""
        return [x for x in instances if x.classification == classification]
        

    def display(self):
        """Displays the tree and test/training accuracy"""
        self.display_tree(self.root,-1)
        self.run_tests()

    def display_tree(self,node,level):
        """Displays the decision tree generated from the ID3 Algorithm"""
        if node:
            s = "{spacing}{name} = {classification} : ".format(
                spacing="| "*level,
                name=node.name,
                classification=node.classification)
                
            if node.value is not None:
                s += "{}".format(node.value)
            #we check if node has a name so we don't display the root node with all instances
            if node.name: 
                print(s)
            
            self.display_tree(node.left,level+1)
            self.display_tree(node.right,level+1)
     
    #Methods involving testing the generated decision tree
    def get_accuracy(self, instances):
        """gets the accuracy result of all the instances""" 
        correct_results = []
        for inst in instances:
            actual = self.get_class_from_tree(inst,self.root)
            expected = inst.classification
            if actual == expected:
                correct_results.append(inst)
          
        accuracy = (len(correct_results) / len(instances)) * 100
        return accuracy
     
    def run_tests(self):
        training_results = round(self.get_accuracy(self.instances),1)
        test_results = round(self.get_accuracy(self.test_instances),1)
         
        print ("Accuracy on training set ({} instances): {}%".format(len(self.instances), training_results))
        print ("Accuracy on test set ({} instances): {}%".format(len(self.test_instances) ,test_results))
    
    def get_class_from_tree(self, instance, cur_node):       
        #To simplify searching for correct instance a attr value 0 will always be on the left branch if exists
        #otherwise we will use the right branch if the attributes value is 1
        #if neither branch exists then we are on a leaf node then we can return the nodes value
        """runs an instance through the tree to check if its classification is correct"""
        left = cur_node.left
        right = cur_node.right
        if left is None and right is None:
            return cur_node.value
        else:
            try:
                attr = left.name
            except AttributeError:
                attr = right.name
                    
            if instance.attributes[attr] == 0 and left is not None:
                return self.get_class_from_tree(instance,left)
            elif instance.attributes[attr] == 1 and right is not None:
                return self.get_class_from_tree(instance,right)        
       
        
def is_pure_node(node):
    """returns true if a node is pure as well as setting value"""
    def most_common_classification(instances):
        """helper for getting the most common classification between instances"""
        values = [x.classification for x in instances]
        return max(set(values), key=values.count)
        
    if len(set([x.classification for x in node.instances])) == 1:
        node.value = node.instances[0].classification
        return True
    
    if not len(node.attributes) > 0:
        node.value = most_common_classification(node.instances)
        return True
    
    return False   