#Main file will be the the entry point for running the program
#it takes 2 command line arguments the training and test information
#it then starts the algorithm and then displays the tree and train/test accuracy

from decision_tree import ID3Algorithm
from sys import argv

train, test = argv[1],argv[2]

algorithm = ID3Algorithm(train,test)
algorithm.start()
algorithm.display()
