# The input is a line of each instance where the last line is the classification of the input
# the input we return will be a list a tuples of each training instance. 
# We parse the input into a class TrainingData which contains instances


class Instance(object):
    """Instance with a tuple of attribute values and a binary classification"""
    def __init__(self, attributes=None,classification=None):
        self.attributes = attributes
        self.classification = classification
        
class TrainingData(object):
    def __init__(self, attr_names):
        self.data = []
        self.attribute_names = attr_names
    
    def add_instance(self,instance):
        self.data.append(instance)    


def get_training_data(fname):
    """convert input file into a TrainingData Set"""

    #remove all empty lines from the file 
    with open(fname) as f:
        data = [line for line in f if not line.isspace()]
    
    #first line of input is the attribute names 
    attr_names = list(data[0].split("\t")[:-1])
    training_data = TrainingData(attr_names)
    
    for line in data[1:]:
        if line:
            instance = get_training_instance(line, attr_names)  
            training_data.add_instance(instance)
    
    return training_data
    
def get_training_instance(line, attr_names):
    values = tuple(map(int, line.split("\t")))
    attributes = {key: val for (key,val) in zip(attr_names,values[:-1])}
    classification = values[-1]
    return Instance(attributes,classification)