import unittest
import parse



class TestParseFunctions(unittest.TestCase):
    def test_get_training_data_returns_correct_instaces(self):
        exp_attr = {"x1" : 1, "x2" : 0, "x3" : 1}
        exp_class = 0
        exp_instance = parse.Instance(exp_attr,exp_class)
        
        line = "1\t0\t1\t0"
        attr_names = ("x1","x2","x3")
        actual_instance = parse.get_training_instance(line,attr_names)
        print (actual_instance.attributes,actual_instance.classification)
        self.assertEqual(exp_instance.attributes,actual_instance.attributes)
        self.assertEqual(exp_instance.classification,actual_instance.classification)




if __name__ == "__main__":
    unittest.main()