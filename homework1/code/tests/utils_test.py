#simple unittests for the files in the utils module
#We are testing that our log2 function returns 0 when we 
#try to calculate log_2(0)
#as well as the normal value for any other log_2(x)
#


import unittest
import utils

class TestUtilsMethods(unittest.TestCase):
    def test_log2_returns0_ifxis0(self):
        self.assertEqual(0,utils.log2(0))
    
    def test_log2_returns_correct(self):
        self.assertEqual(1.0,utils.log2(2))
        self.assertEqual(2.0,utils.log2(4))
        
        
    def test_entropy(self):
        self.assertEqual(0,utils.entropy([1]))
        self.assertEqual(1.0,utils.entropy([0.5,0.5]))
        self.assertEqual(.72,utils.entropy([.80, .20]))
        
        
if __name__ == "__main__":
    unittest.main()