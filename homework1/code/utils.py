# The utils.py will contain some some common functions used througout 
# the program. Since we need a log_2 function to return 0 when we have a 0 number
# we need a custom log_2 function called log2.
# The entropy function will calculate the entropy of a given list of propabilities.
# and rounded to 2 decimal places
from math import log


def entropy(probs):
    return sum([-x*log2(x) for x in probs])
    
    
#because log_2(0) throws a domain error we define it here as 0
#for special cases 
#in our case any valueerror will return a 0
def log2(x):
    """returns log_2(x) if x is 0 return 0"""
    try:
        res = log(x,2)
    except ValueError:
        return 0
    else:
        return res            