This a simple perceptron that will train a data with n binary attributes as well as a binary 
classification. The Program is written in python 3.5 on a windows platform however should be able to run on any 
python 3.5 installation. 


Running the Program
After unzipping homework2 archive you should have the follow structure

homework2:
    code\
        main.py
        parse.py
        perceptron.py
        utils.py
    data\
        train.dat
        test.dat
        train2.dat
        test2.dat
    results\
    
    
To run the code cd to homework2 and type following
python code\main.py <path\to\train.dat> <path\to\test.dat> <learning_rate> <iterations>

