from sys import argv
from perceptron import Perceptron
from parse import get_training_data

script, train, test, rate, iter = argv

rate = float(rate)
iter = int(iter)

train_data = get_training_data(train)
test_data = get_training_data(test)

keys = test_data.attribute_names

perceptron = Perceptron(keys=keys, rate=rate)                    
perceptron.learn(iter,train_data)

res = perceptron.test_perceptron(train_data)
print ("Accuracy on training set({} instances): {}%".format(len(train_data.data),res))
res = perceptron.test_perceptron(test_data)
print ("Accuracy on test set({} instances): {}%".format(len(test_data.data),res))