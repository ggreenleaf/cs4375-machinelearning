from utils import dot, sig,sig_prime
from collections import OrderedDict


def dot(weights, inputs):
    """The dot product of 2 1D lists"""
    return sum(weights[key] * inputs[key] for key in weights.keys())


class Perceptron(object):
    def __init__(self,keys=[], func = sig, func_deriv = sig_prime, rate = .5):
        self.weights = OrderedDict()
        self.act_function = func
        self.gradient_func = sig_prime
        self.threshold = 0.5
        self.rate = rate
  
        
        
    def run_instance(self, inputs):
        """Runs a single instance through the perceptron"""
        dot_product = dot(self.weights, inputs)
        out = self.act_function(dot_product)
        return out

    def get_inputs_classification(self,instances, i):
        """Grabs the ith input,classification from instances"""
        instance = instances.data[i % len(instances.data)]
        inputs = instance.attributes
        classification = instance.classification
        return inputs, classification
    
    
    def weight_string(self):
        """Returns a formated string of weights"""
        ls = ["w({})={:2.4f}".format(key,self.weights[key]) for key in self.weights.keys()]
        return " ".join(ls)
    
    def learn(self, iterations, instances):
        """The learning algorithm for a sigmoid perceptron"""
        for key in instances.data[0].attributes.keys():
            self.weights[key] = 0 
        
        
        for i in range(iterations):
            inputs, expected = self.get_inputs_classification(instances, i)
            result = self.run_instance(inputs)
            error = self.get_error(expected, result)
            self.update_weights(inputs, error)
            ws = self.weight_string()
            print ("After Iteration {}: {} output={:2.4f}".format(i,ws,self.run_instance(inputs)))
                   
            # self.display(i,self.run_instance(inputs))       
    
    def test_perceptron(self, instances):
        """Gets the accuracy of instances based of a learned perceptron"""
        def nxor(out, expected):
            if out >= .5 and bool(expected):
                return True
            elif (out < .5 and not bool(expected)):
                return True
            return False
        results = []
        for i in range(len(instances.data)):
            inputs, classification = self.get_inputs_classification(instances, i)
            results.append(nxor(self.run_instance(inputs),classification))   
        
        accuracy = (float(results.count(True)) / len(results)) * 100
        return accuracy
    
    
    def get_error(self, y, actual):
        """Gets the error between the actual and expected outputs"""
        error = y - actual
        return error  
   
    def update_weights(self,inputs,error):
        """using the weight update rule update all the weights of the perceptron"""
        dp = dot(self.weights, inputs)
        for key in self.weights.keys():
            self.weights[key] += (self.rate * error * inputs[key] * sig_prime(dp))     