from math import exp

def sig(x):
    """The sigmoid function 1 / 1+e^x"""
    return (1 / (1 + exp(-x)))
    
def sig_prime(x):
    """The derivative of the sigmoid function"""
    return sig(x)*(1 - sig(x))
    # return round((exp(x) / ((exp(x) + 1)**2)),4)  
    
    
def dot(a,b):
    """the dot product of 2 lists a and b"""
    return sum([x*y for x,y in zip(a,b)])
    

