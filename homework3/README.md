A simple value iteration program for solving MDPs.
 The Program is written in python 3.5 on a windows platform however should be able to run on any 
python 3.5 installation. 


Running the Program
After unzipping homework2 archive you should have the follow structure

homework2:
    code\
        main.py
        parse.py
        mdp.py
        utils.py
    data\
        test.in
        test2.in
        test3.in
        
    results\
    
    
To run the code cd to homework3 and type following
python code\main.py <num_states> <num_actions> <path/to/test.in> <discount_factor>

