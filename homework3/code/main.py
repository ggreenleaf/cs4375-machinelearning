from sys import argv
from mdp import MDP


script, num_states, num_actions, filename, discount_factor = argv

mdp = MDP(filename, float(discount_factor), int(num_actions))

mdp.value_iteration()