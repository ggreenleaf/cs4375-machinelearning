from parse import load_states

class MDP(object):
    def __init__(self, filename, discount_factor, num_actions):
        self.states = load_states(filename)
        self.discount_factor = discount_factor
        self.num_actions = num_actions

    def value_iteration(self):
        """The value iteration algorithm ran for 20 iterations"""
        for i in range(20):
            j_vals = []
            row = []
            #hack way to display states inorder change in future updates
            for key in sorted(self.states.keys(), key=lambda k: int(''.join(c for c in k if c.isdigit()))):
                action, j = self.bellmans(self.states[key])
                res = "({} a{} {:.4f})".format(key, action, j)
                row.append(res)
                j_vals.append((key, j))
                
            self.update_j(j_vals)    
            print ("After iteration {}:".format(i+1))
            print (" ".join(row))  
    
    def update_j(self, j_vals):
        """Update the j values of all states"""
        for key, j in j_vals: 
            self.states[key].j = j
            
    def bellmans(self,state):
        """Bellmans equation returning the max j value along with action"""
        actions = []
        for action in list(range(1,self.num_actions + 1)):
            if "a{}".format(action) in state.transitions.keys():
                t = (action, state.reward + self.discount_factor * self.expected_value(state,action))
                actions.append(t)

        return max(actions, key=lambda x: x[1])
    
    def expected_value(self, state, action): 
        """Expected value of transitioning from state with action"""
        keys = self.states.keys()
        action = "a{}".format(action)
        weighted_probs = []
        for key in keys:
            weighted_probs.append(state.transitions[action][key] * self.states[key].j)
        print
        return sum(weighted_probs)