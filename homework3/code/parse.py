from collections import defaultdict
import re 

class State(object):
    """A State in a Markov (MDP) system
    
    Attributes:
        reward (float): the reward gained from reaching this state
        transitions (defaultdict): nested default dict defining the transition with action to state with propability
        j (float): the best possible reward 
        
    """
    def __init__(self, name = "", reward = 0):
        self.name = name
        self.reward = reward
        self.transitions = defaultdict(lambda : defaultdict(float))
        self.j = 0
        
    def create_transitions(self, transitions):
        pattern = re.compile(r"\(.+?\)")
        for transition in re.findall(pattern, transitions):
            action, state, prob = transition[1:-1].split(" ")
            self.transitions[action][state] = float(prob)
   
def load_states(filename):
    """loads the file and parses the input into states"""
    states = defaultdict(State)
    with open(filename) as f:
        data = [line for line in f if not line.isspace()]
    for line in data:
        state = create_state(line)
        states[state.name] = state
    return states
def create_state(line):
    """creates a state from a string <name> <reward> (<state transitions>)"""
    name, reward, transitions = line.split(" ",2)
    state = State(name, float(reward))
    state.create_transitions(transitions)
    return state


    

