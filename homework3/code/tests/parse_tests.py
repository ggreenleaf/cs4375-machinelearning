import unittest
from code import parse


class TestParseFunctions(unittest.TestCase):
    def test_state_adds_transition_correctly(self):
        state = parse.State("S1",0)
        transitions = "(a1 s1 0.5) (a1 s2 0.5) (a2 s1 1.0)"
        state.create_transitions(transitions)
        self.assertEqual(state.transitions["a1"]["s1"],0.5)
        self.assertEqual(state.transitions["a1"]["s2"],0.5)
        self.assertEqual(state.transitions["a2"]["s1"],1.0)
    
    def test_state_created_successfully(self):
        s = "s3 10 (a1 s2 1.0) (a2 s3 0.5) (a2 s4 0.5)"
        state = parse.create_state(s)
        self.assertEqual(state.reward, 10)
        self.assertEqual(state.name, "s3")
        
        #checking if transitions were created correctly
        self.assertEqual(state.transitions["a1"]["s2"], 1.0)
        self.assertEqual(state.transitions["a2"]["s3"], 0.5)
        self.assertEqual(state.transitions["a2"]["s4"], 0.5)
    

if __name__ == "__main__":
    unittest.main()