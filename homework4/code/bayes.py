class Bayes(object):
    def __init__(self, attribute_names, train, test):
        self.train = train
        self.test = test
        self.names = attribute_names
        self.n = len(attribute_names)
        self.class_probs = [] #list of p(Class=x)
        self.cond_given_class = [[],[]] #list of tuples where each tuple is the p(A=x | C=y)
    
    def learn(self):
        """learns all the conditional probabilities for the given attributes"""
        for j in range(2):
            c_prob = self.get_classification_probability(j)
            ps = ["P(C={})".format(c_prob)]
            self.class_probs.append(c_prob)
            for i in range(self.n):
                name = self.names[i]
                attr_probs = [self.p_attr_given_c(j,i,b) for b in range(2)]
                ps.extend([self.print_p(name,k, j, p) for k,p in enumerate(attr_probs)])
                self.cond_given_class[j].append(tuple(attr_probs))
            print (" ".join(ps))        
  
                
    def get_classification_probability(self, c):
        """get the probability of an instance being having a classification c"""
        return round(len([x for x in self.train if x[1] == c]) / len(self.train),2)
        
    def p_attr_given_c(self, c, attr, expected_attr):
        """given a certain class calculate the probability of a attr occuring"""
        list_by_c = [x for x in self.train if x[1] == c]
        num_attr = len([x for x in list_by_c if x[0][attr] == expected_attr])
        return round(num_attr / len(list_by_c),2)
    
    def print_p(self,attr, attr_value, c, p,):
        """creates a string for displaying probability"""
        s = "P({}={} | C={})={}".format(attr,attr_value,c,p)
        return s
    
    def test_data(self, data="test"):
        """tests the accuracy of our naive classifier"""
        instances = self.test
        res_string = "Accuracy on test set ({} instances): ".format(len(self.test))
        if data == "train":
            instances = self.train
            res_string = "Accuracy on train set ({} instances): ".format(len(self.train))
        
        num_correct = 0
        for i in instances:
            expected = i[1]
            actual = self.get_classification(i)    
            if expected == actual:
                num_correct += 1
        accuracy = (num_correct / len(instances)) * 100
        res_string += str(round(accuracy,1)) + "%"
        print (res_string)            
                         
    def get_classification(self, instance):
        """Gets the classification using our naive classifier"""
        p0 = self.get_prob_given_class(instance[0], 0)
        p1 = self.get_prob_given_class(instance[0], 1)
        return int(p0 < p1) #return 0 if p0 > p1 else return 1
    
    def get_prob_given_class(self, attrs, c):
        """calculates the probability of p(attrs | c) assuming conditional independence"""
        res = self.class_probs[c]
        for i in range(len(attrs)):
            res *= self.cond_given_class[c][i][attrs[i]]
        return res
        