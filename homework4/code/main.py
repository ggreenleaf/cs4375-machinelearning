from sys import argv
from parse import get_training_data
from bayes import Bayes

script, train_file, test_file = argv

attribute_names, train = get_training_data(train_file)
attribute_names, test = get_training_data(test_file)

b = Bayes(attribute_names, train, test)
b.learn()
b.test_data("train")
b.test_data("test")