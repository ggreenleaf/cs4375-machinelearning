def get_training_data(fname):    
    #remove all empty lines from the file 
    with open(fname) as f:
        data = [line for line in f if not line.isspace()]
    
    #first line of input is the attribute names 
    attribute_names = list(data[0].split("\t")[:-1])
    instances = []
    
    instances = [parse_line(line) for line in data[1:]]
    return  attribute_names, instances
    
def parse_line(line):
    values = list(map(int, line.split("\t")))
    classification = values[-1]
    
    return [values[:-1], classification]